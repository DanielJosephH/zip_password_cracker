USAGE
------------------------------------------------------------------

python2 zip_pass_cracker.py zip_file.zip [-h] [-l=max_length]

use -h to see full usage information.

-l option specifies the maximum length of password to try for brute_crack brute
force approach. default value is 6.


References
------------------------------------------------------------------

1000 most common passwords taken from "DavidWittman"

https://github.com/DavidWittman/wpxmlrpcbrute/blob/master/wordlists/1000-most-common-passwords.txt

import zipfile
import argparse

# manage arguments
parser = argparse.ArgumentParser(description="Crack the password for a locked zip file.")
parser.add_argument("zip", help="The locked zip file to obtain the password of.")
parser.add_argument("-l", "--length",dest="length", type=int, default=6, help="The maximum length password to try using brute force.")

args = parser.parse_args()

# try unzipping the file with every common password in the list
def list_crack(my_zip):

    try:
        f = open("1000CommonPasswords.txt", "r")
    except IOError:
        print "ERROR: Password dictionary cannot be read"

    # try each password in the password dictionary
    for line in f:
        password = line.strip("\n")
        try:
            my_zip.extractall(pwd = password)
            password_found = True
            f.close()
            return password
            break
        except:
            pass

    f.close()
    return None

def brute_crack(my_zip, length):

    possible_chars = list("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#%")
    password = [' ' for x in range(length)]
    password[length - 1] = 'a'
    counter = 0
    index = len(password) - 1

    while (True):

        # count up from the right hand side
        try:
            my_zip.extractall(pwd = ''.join(password).strip(' '))
            return ''.join(password).strip(' ')
        except:
            pass

        counter += 1

        # if the char is at the end of the alphabet, set it back to the first char and increment the next char
        while counter >= len(possible_chars):
            password[index] = possible_chars[0]
            # move left and increment to next chars
            index -= 1
            if index < 0:
                # completed all permutations up to max length
                return None
            if password[index] == ' ':
                counter = 0
            else:
                counter = possible_chars.index(password[index]) + 1

        password[index] = possible_chars[counter]
        if index != len(password) - 1:
            index = len(password) - 1
            counter = 0

def main():

    # open the zip file
    try:
        my_zip2 = zipfile.ZipFile(args.zip)
    except zipfile.BadZipfile:
        print "ERROR, tried to read bad zip file"
    except IOError:
        print "ERROR, an error occured trying to read the file"
        exit(0)

    # try using a list of passwords first, then brute force if password not found
    password = list_crack(my_zip2)
    if (password is not None):
        print "password is:" + password
    else:
        password = brute_crack(my_zip2, args.length)
        if (password is not None):
            print "password is:" + password
        else:
            print "Password not found"

    my_zip2.close()

if __name__ == "__main__":
    main()
